# tracie_deploy

Setup:

```
mkdir mesa_dashboards
cd mesa_dashboards
git clone git@gitlab.freedesktop.org:saavedra.pablo/mesa_dashboards_ansible.git ansible
```

Run:

```
ansible-playbook -i hosts mesadash.yml --diff
```

where `hosts`:

```
[mesaci_nginx]
mesadash ansible_ssh_host=192.168.0.10

[mesaci_apps]
mesadash ansible_ssh_host=192.168.0.11

[mesaci_db]
mesadash ansible_ssh_host=192.168.0.12

[tracie_nginx]
mesadash ansible_ssh_host=192.168.0.13

[tracie_apps]
mesadash ansible_ssh_host=192.168.0.14

[tracie_db]
mesadash ansible_ssh_host=192.168.0.15
```

and `mesadash.yml`:

```
- hosts:
    - mesaci_db
  roles:
    - { role: ansible/roles/mesacidb }

- hosts:
    - mesaci_nginx
  roles:
    - { role: ansible/roles/mesacinginx }

- hosts:
    - mesaci_apps
  roles:
    - { role: ansible/roles/mesaciapp }

- hosts:
    - tracie_db
  roles:
    - { role: ansible/roles/traciedb }

- hosts:
    - tracie_nginx
  roles:
    - { role: ansible/roles/tracienginx }

- hosts:
    - tracie_apps
  roles:
    - { role: ansible/roles/tracieapp }
```
