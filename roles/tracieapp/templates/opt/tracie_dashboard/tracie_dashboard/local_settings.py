# flake8: noqa
# {{ ansible_managed }}

ALLOWED_HOSTS = [
    "{{ tracie_app_server_name }}"
]

{{ tracie_app_local_settings_custom }}
